function applyCustomAnswers(common, custom) {
    if (typeof custom !== 'undefined') {
        var merged = {};
        for (var key in common) {
            if (Object.prototype.toString.call(common[key]) === '[object Object]') {
                merged[key] = applyCustomAnswers(common[key], custom[key]);
            } else if (custom[key]) {
                merged[key] = custom[key];
            } else {
                merged[key] = common[key];
            }
        }
        if (merged){
            return merged;
        } else {
            return {"1":"2"};
        }
        
    }
    return common;
}