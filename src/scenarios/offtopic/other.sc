theme: /Offtopic

    state: YouMisunderstoodMe
        q!: * $you * [меня] * (не [так]|неправильно) (понимае*|понял*|поняли) * 
        q!: * {(недовол*|не удовлетвор*|не понравил*) [$AnyWord] [$AnyWord] (бесед*|диалог*|общение*)} *
        a: {{$global.OfftopicAnswers["YouMisunderstoodMe"]}}

    state: Test || noContext = true
        q!: (тест|прием|есть контакт|проверка связи) [прием]
        a: {{$global.OfftopicAnswers["Test"]}}
        go!: /Offtopic/HowCanIHelpYou?

    state: MyNameIs || noContext = true
        q!: {[$AnyWord] [$AnyWord] меня зовут [$AnyWord] [$AnyWord]}
        q!: * {[$AnyWord] [$AnyWord] $my (имя|прозвище|кличка|ник|никнейм|ник-нейм|ник нейм) [$AnyWord] [$AnyWord] }
        a: {{$global.OfftopicAnswers["MyNameIs"]}}
        go!: /Offtopic/HowCanIHelpYou?

    state: WaitAMoment || noContext = true
        q!: {(*ждите|подожд*) [одну|пару] * (минут*|секунд*|немного|немнож*)}
        q!: {(одну|пару) * (минут*|секунд*)}
        q!: (минут*|секунд*)
        q!:  не (отключай*|переключай*|переключай*|отсодиняй*) 
        a: {{$global.OfftopicAnswers["WaitAMoment"]}}